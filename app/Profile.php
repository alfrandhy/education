<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = [
        'user_id','nuptk','no_seri_karpeg','pangkat','golongan','ruang','tmt_pangkat','jabatan','tmt_jabatan','tanggal_lahir','tempat_lahir','pendidikan','pendidikan_jurusan','jabatan_fungsional','tmt_jabatan_fungsional','masa_kerja_lama','masa_kerja_baru','jenis_guru','mengajar','unit_kerja','gaji_lama','gaji_baru','jumlah_ak_lama','jumlah_ak_baru','alamat','provinsi','kota','kelurahan','kecamatan','kode_pos',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
