<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\Kepangkatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $userId = $id ?: auth()->user()->id;
        $user = User::with('profile')->findOrFail($userId);
        return view('profile.index', compact('user','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return route('profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null, User $user, Profile $profile)
    {
        $userId = $id ?: auth()->user()->id;
        $user = User::with('profile')->findOrFail($userId);
        
        // $request->validate([
        //     'name' => ['required', 'string'],
        //     'nip' => ['required', 'string'],
        //     'jk' => ['required', 'string'],
        //     'email' => ['required', 'string'],
        //     'nuptk' => ['required','string'],
        //     'no_seri_karpeg' => ['required','string'],
        //     'pangkat' => ['required','string'],
        //     'golongan' => ['required','string'],
        //     'ruang' => ['required','string'],
        //     'tmt_pangkat' => ['required','string'],
        //     'jabatan' => ['required','string'],
        //     'tmt_jabatan' => ['required','string'],
        //     'tanggal_lahir' => ['required','string'],
        //     'tempat_lahir' => ['required','string'],
        //     'pendidikan' => ['required','string'],
        //     'pendidikan_jurusan' => ['required','string'],
        //     'jabatan_fungsional' => ['required','string'],
        //     'tmt_jabatan_fungsional' => ['required','string'],
        //     'masa_kerja_lama' => ['required','string'],
        //     'masa_kerja_baru' => ['required','string'],
        //     'jenis_guru' => ['required','string'],
        //     'mengajar' => ['required','string'],
        //     'unit_kerja' => ['required','string'],
        //     'gaji_lama' => ['required','string'],
        //     'gaji_baru' => ['required','string'],
        //     'jumlah_ak_lama' => ['required','string'],
        //     'jumlah_ak_baru' => ['required','string'],
        //     'alamat' => ['required','string'],
        //     'provinsi' => ['required','string'],
        //     'kota' => ['required','string'],
        //     'kelurahan' => ['required','string'],
        //     'kecamatan' => ['required','string'],
        //     'kode_pos' => ['required','string'],
        // ]);
        $rules = array_map(function () {
            return ['required'];
        }, $request->all());
        
        $request->validate($rules);
        
        $user = User::update($request->all());
        $user->profile = Profile::update($request->all());
        $user->save();
        return route('profile')->with('success','Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
