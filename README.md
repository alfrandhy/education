<div id="list">
Daftar Isi
</div>

<div id="about">
I About App
</div>

<div id="instalation">
II Instalasi
</div>

1. Clone From Github 
    git clone https://alfrandhy@bitbucket.org/alfrandhy/education.git
2. Install Bootsrap Template with auth
    2.1. composer require laravel/ui
    2.2. php artisan ui bootstrap --auth
    2.3. npm install && npm run dev
3. Costumize
    3.1. Change username as nip for login {vendor\laravel\ui\auth-backend\AuthenticatesUsers.php}
    3.2. Comment $this->guard()->login($user); {vendor\laravel\ui\auth-backend\RegisterUser.php}
    3.3. change {vendor\laravel\ui\auth-backend\RegistersUsers.php}
        public function showRegistrationForm()
        {
            $pangkat = App\Kepangkatan::get();
            return view('auth.register',['pangkat',$pangkat]);
        }
    
