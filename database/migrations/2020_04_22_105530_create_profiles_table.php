<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('nuptk')->nullable();
            $table->string('no_seri_karpeg')->nullable();
            $table->string('pangkat')->nullable();
            $table->string('golongan')->nullable();
            $table->string('ruang')->nullable();
            $table->string('tmt_pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('tmt_jabatan')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->string('pendidikan')->nullable();
            $table->string('pendidikan_jurusan')->nullable();
            $table->string('jabatan_fungsional')->nullable();
            $table->string('tmt_jabatan_fungsional')->nullable();
            $table->string('masa_kerja_lama')->nullable();
            $table->string('masa_kerja_baru')->nullable();
            $table->string('jenis_guru')->nullable();
            $table->string('mengajar')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->string('gaji_lama')->nullable();
            $table->string('gaji_baru')->nullable();
            $table->string('jumlah_ak_lama')->nullable();
            $table->string('jumlah_ak_baru')->nullable();
            $table->string('alamat')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
