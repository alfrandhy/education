@extends('layouts.adminlte')

@section('subtitle','Profile')

@section('css')

@section('js')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
<li class="breadcrumb-item active">Profile</li>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Description Horizontal
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-6">Masa Penilaian</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Nama</dt>
                  <dd class="col-sm-6">{{ $user->name }}</dd>
                  <dt class="col-sm-6">NIP</dt>
                  <dd class="col-sm-6">{{ $user->nip }}</dd>
                  <dt class="col-sm-6">NUPTK</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Nomor Seri KARPEG</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Pangkat / Golongan Ruang / TMT</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jabatan / TMT</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Tempat dan Tanggal Lahir</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jenis Kelamin</dt>
                  <dd class="col-sm-6">{{ $user->jk }}</dd>
                  <dt class="col-sm-6">Pendidikan Tertinggi</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jabatan Fungsional / TMT</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Masa Kerja Golongan Lama</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Masa Kerja Golongan Baru</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jenis Guru</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Mengajar Kelas</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Unit Kerja</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Gaji Pokok Lama</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Gaji Pokok Baru</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jabatan/TMT/Jumlah AK Lama</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                  <dt class="col-sm-6">Jabatan/TMT/Jumlah AK Baru</dt>
                  <dd class="col-sm-6">NUPTK</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Pengajuan
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-6">Tanggal Pengajuan</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Untuk Kenaikan Jabatan</dt>
                  <dd class="col-sm-6">{{ $user->name }}</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">{{ $user->nip }}</dd>
                  <dt class="col-sm-6">Pangkat/Golongan Ruang</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">TMT</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Data Kepala Sekolah
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-6">Tanggal Pengajuan</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Untuk Kenaikan Jabatan</dt>
                  <dd class="col-sm-6">{{ $user->name }}</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">{{ $user->nip }}</dd>
                  <dt class="col-sm-6">Pangkat/Golongan Ruang</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">TMT</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Data Kepala Dinas
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-6">Tanggal Pengajuan</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">Untuk Kenaikan Jabatan</dt>
                  <dd class="col-sm-6">{{ $user->name }}</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">{{ $user->nip }}</dd>
                  <dt class="col-sm-6">Pangkat/Golongan Ruang</dt>
                  <dd class="col-sm-6">Null</dd>
                  <dt class="col-sm-6">TMT</dt>
                  <dd class="col-sm-6">TMT</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
@stop