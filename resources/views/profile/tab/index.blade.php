<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">
      <i class="fas fa-text-width"></i>
      Description
    </h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <dl class="row">
      <dt class="col-sm-6">Nama</dt>
      <dd class="col-sm-6">{{ $user->name }}</dd>
      <dt class="col-sm-6">NIP</dt>
      <dd class="col-sm-6">{{ $user->nip }}</dd>
      <dt class="col-sm-6">NUPTK</dt>
      <dd class="col-sm-6">{{ $user->profile->nuptk }}</dd>
      <dt class="col-sm-6">Nomor Seri KARPEG</dt>
      <dd class="col-sm-6">{{ $user->profile->no_seri_karpeg }}</dd>
      <dt class="col-sm-6">Pangkat / Golongan Ruang / TMT</dt>
      <dd class="col-sm-6">NULL</dd>
      <dt class="col-sm-6">Jabatan / TMT</dt>
      <dd class="col-sm-6">NULL</dd>
      <dt class="col-sm-6">Tempat dan Tanggal Lahir</dt>
      <dd class="col-sm-6">{{ $user->profile->tempat_lahir }}/{{ $user->profile->tanggal_lahir }}</dd>
      <dt class="col-sm-6">Jenis Kelamin</dt>
      <dd class="col-sm-6">{{ $user->jk }}</dd>
      <dt class="col-sm-6">Pendidikan Tertinggi</dt>
      <dd class="col-sm-6">{{ $user->profile->pendidikan }}{{ $user->profile->pendidikan_jurusan }}</dd>
      <dt class="col-sm-6">Jabatan Fungsional / TMT</dt>
      <dd class="col-sm-6">{{ $user->profile->jabatan_fungsional }}/{{ $user->profile->tmt_jabatan_fungsional }} </dd>
      <dt class="col-sm-6">Masa Kerja Golongan</dt>
      <dd class="col-sm-6">{{ $user->profile->masa_kerja_lama }}</dd>
      <dt class="col-sm-6">Jenis Guru</dt>
      <dd class="col-sm-6">{{ $user->profile->jenis_guru }}</dd>
      <dt class="col-sm-6">Mengajar Kelas</dt>
      <dd class="col-sm-6">{{ $user->profile->lama_mengajar }}</dd>
      <dt class="col-sm-6">Unit Kerja</dt>
      <dd class="col-sm-6">{{ $user->profile->unit_kerja }}</dd>
      <dt class="col-sm-6">Gaji Pokok Lama</dt>
      <dd class="col-sm-6">{{ $user->profile->gaji_lama }}</dd>
      <dt class="col-sm-6">Jabatan/TMT/Jumlah AK Lama</dt>
      <dd class="col-sm-6">{{ $user->profile->jumlah_ak_lama }}</dd>
    </dl>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">
      <i class="fas fa-text-width"></i>
      Tempat Tinggal
    </h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <dl class="row">
      <dt class="col-sm-6">Alamat</dt>
      <dd class="col-sm-6">{{ $user->profile->alamat }}</dd>
      <dt class="col-sm-6">Provinsi</dt>
      <dd class="col-sm-6">{{ $user->profile->provinsi }}</dd>
      <dt class="col-sm-6">Kota</dt>
      <dd class="col-sm-6">{{ $user->profile->kota }}</dd>
      <dt class="col-sm-6">Kelurahan</dt>
      <dd class="col-sm-6">{{ $user->profile->kelurahan }}</dd>
      <dt class="col-sm-6">Kecamatan</dt>
      <dd class="col-sm-6">{{ $user->profile->kecamatan }}</dd>
      <dt class="col-sm-6">Kode Pos</dt>
      <dd class="col-sm-6">{{ $user->profile->kode_pos }}</dd>
    </dl>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->