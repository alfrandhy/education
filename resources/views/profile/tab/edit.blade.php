<form method="POST" action="{{ route('profile.update') }}">
    @csrf
    <div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">
        <i class="fas fa-text-width"></i>
        Description
        </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <dl class="row">
        <dt class="col-sm-4">Nama</dt>
        <dd class="col-sm-6"><input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">NIP</dt>
        <dd class="col-sm-6"><input type="text" name="nip" class="form-control" value="{{ $user->nip }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">NUPTK</dt>
        <dd class="col-sm-6"><input type="text" name="nuptk" class="form-control" value="{{ $user->profile->nuptk }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Nomor Seri KARPEG</dt>
        <dd class="col-sm-6"><input type="text" name="no_seri_karpeg" class="form-control" value="{{ $user->profile->no_seri_karpeg }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Pangkat / Golongan Ruang / TMT</dt>
        <dd class="col-sm-2">
            <select name="pangkat" class="form-control" value="{{ $user->profile->pangkat }}">
                <option value="">Pilih Data</option>
                <option value="Penata Muda">Penata Muda</option>
                <option value="Penata Muda Tk.I">Penata Muda Tk.I</option>
                <option value="Penata">Penata</option>
                <option value="Penata Tk.I">Penata Tk.I</option>
                <option value="Pembina">Pembina</option>
                <option value="Pembina Tk.I">Pembina Tk.I</option>
                <option value="Pembina Utama Muda">Pembina Utama Muda</option>
                <option value="Pembina Utama Madya">Pembina Utama Madya</option>
                <option value="Pembina Utama">Pembina Utama</option>
            </select>
        </dd>
        <dd class="col-sm-2">
            <select name="golongan" class="form-control" value="{{ $user->profile->golongan }}">
                <option value="">Pilih Data</option>
                <option value="III">III</option>
                <option value="IV">IV</option>
            </select>
        </dd>
        <dd class="col-sm-2">
            <select name="golongan" class="form-control" value="{{ $user->profile->ruang }}">
                <option value="">Pilih Data</option>
                <option value="a">a</option>
                <option value="b">b</option>
                <option value="c">c</option>
                <option value="d">d</option>
                <option value="e">e</option>
            </select>
        </dd>
        <dd class="col-sm-2">
            <input type="text" name="tmt_pangkat" class="form-control" value="{{ $user->profile->tmt_pangkat }}" placeholder="Masukkan Data Anda">
        </dd>
        <dt class="col-sm-4">Jabatan / TMT</dt>
        <dd class="col-sm-3">
            <select name="jabatan" class="form-control" value="{{ $user->profile->jabatan }}">
                <option value="">Pilih Data</option>
                <option value="Guru Pertama">Guru Pertama</option>
                <option value="Guru Muda">Guru Muda</option>
                <option value="Guru Madya">Guru Madya</option>
                <option value="Guru Utama">Guru Utama</option>
            </select>
        </dd>
        <dd class="col-sm-3">
            <input type="text" name="tmt_jabatan" class="form-control" value="{{ $user->profile->tmt_jabatan }}" placeholder="Masukkan Data Anda">
        </dd>
        <dt class="col-sm-4">Tempat dan Tanggal Lahir</dt>
        <dd class="col-sm-3"><input type="text" name="" class="form-control" value="{{ $user->profile->tempat_lahir }}" placeholder="Masukkan Data Anda"></dd>
        <dd class="col-sm-3"><input type="text" name="" class="form-control" value="{{ $user->profile->tanggal_lahir }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Jenis Kelamin</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->jk }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Pendidikan Tertinggi</dt>
        <dd class="col-sm-3">
            <select name="pendidikan_jurusan" class="form-control" value="{{ $user->profile->pendidikan_jurusan }}">
                <option value="">Pilih Data</option>
                <option value="SD">SD</option>
                <option value="SMP">SMP</option>
                <option value="SMA/SMK">SMA/SMK</option>
                <option value="D-I">D-I</option>
                <option value="D-II">D-II</option>
                <option value="D-IV">D-IV</option>
                <option value="S-I">S-I</option>
                <option value="S-II">S-II</option>
                <option value="S-III">S-III</option>
            </select>
        </dd>
        <dd class="col-sm-3">
            <input type="text" name="pendidikan_jurusan" class="form-control" value="{{ $user->profile->pendidikan_jurusan }}" placeholder="Masukkan Data Anda"> 
        </dd>
        <dt class="col-sm-4">Jabatan Fungsional / TMT</dt>
        <dd class="col-sm-3">
            <input type="text" name="jabatan_fungsional" class="form-control" value="{{ $user->profile->jabatan_fungsional }}" placeholder="Masukkan Data Anda"> 
        </dd>
        <dd class="col-sm-3">
            <input type="text" name="tmt_jabatan_fungsional" class="form-control" value="{{ $user->profile->tmt_jabatan_fungsional }}" placeholder="Masukkan Data Anda"> 
        </dd>
        <dt class="col-sm-4">Masa Kerja Golongan Lama</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->masa_kerja_lama }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Jenis Guru</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->jenis_guru }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Mengajar Kelas</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->lama_mengajar }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Unit Kerja</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->unit_kerja }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Gaji Pokok Lama</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->gaji_lama }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-4">Jabatan/TMT/Jumlah AK Lama</dt>
        <dd class="col-sm-6"><input type="text" name="" class="form-control" value="{{ $user->profile->jumlah_ak_lama }}" placeholder="Masukkan Data Anda"></dd>
        </dl>
    </div>
    <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">
        <i class="fas fa-text-width"></i>
        Tempat Tinggal
        </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <dl class="row">
        <dt class="col-sm-3">Alamat</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->alamat }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-3">Provinsi</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->provinsi }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-3">Kota</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->kota }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-3">Kelurahan</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->kelurahan }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-3">Kecamatan</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->kecamatan }}" placeholder="Masukkan Data Anda"></dd>
        <dt class="col-sm-3">Kode Pos</dt>
        <dd class="col-sm-8"><input type="text" name="" class="form-control" value="{{ $user->profile->kode_pos }}" placeholder="Masukkan Data Anda"></dd>
        </dl>
    </div>
    <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <button type="submit" class="btn btn-lg btn-success">Perbaharui Data</button>
</form>